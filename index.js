const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//change the port to the ika url
app.listen(64801, () => console.log('Webhook server is listening, port 64801'));


var verificationController = require('./controllers/verification');
var messageWebhookController = require('./controllers/messageWebhook');

app.get('/', verificationController);
app.post('/', messageWebhookController);





